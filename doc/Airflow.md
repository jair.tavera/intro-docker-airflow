## 🔰  Apache Airflow: Orquestación y Automatización de Flujos de Trabajo

**Apache Airflow es una plataforma de código abierto para la programación y gestión de flujos de trabajo de datos.** 
Se utiliza para orquestar tareas, como la carga de datos, la limpieza de datos, la transformación de datos y el análisis de datos.

Airflow ayuda a ingenieros de datos a definir, programar y monitorizar cualquier workflow o 
pipeline de procesamiento de información de manera fácil y portable entre diferentes stacks tecnológicos. 
Es ampliamente utilizado por compañías como **Google, Airbnb, Lyft** para sus pipelines de _**Big Data.**_

## Arquitectura

📗 **Airflow** es un conjunto de varios componentes que trabajan juntos para ejecutar flujos de trabajo de forma orquestada:

![Airflow Arquitectura](images/airflow_arq.jpg)

## Componentes

▶️ **Servidor web:**

En ella podemos administrar y monitorizar flujos de trabajo, tareas y ejecuciones.

![Airflow Arquitectura](images/airflow_dags.png)

▶️ **Scheduler:**

-   Componente central que se encarga de ejecutar los flujos de trabajo según su programación.
-   Monitoriza los flujos de trabajo y sus dependencias para determinar qué tareas deben ejecutarse.
-   Envía las tareas a los workers para su ejecución.

▶️ **Worker:**

-   Procesos que se encargan de ejecutar las tareas de los flujos de trabajo.
-   Se ejecutan en diferentes máquinas y se comunican con el scheduler para recibir tareas.
-   Ejecutan las tareas utilizando los recursos disponibles en la máquina.

▶️ **Base de datos:**

-   Almacena la metadata de Airflow, como flujos de trabajo, tareas, ejecuciones y usuarios.
-   Se puede usar una base de datos relacional como PostgreSQL o MySQL, o una NoSQL como Apache Cassandra.

▶️ **CLI (Command Line Interface):**

-   Interfaz de línea de comandos para interactuar con Airflow desde la terminal.
-   Permite ejecutar comandos para administrar flujos de trabajo, tareas y ejecuciones.

▶️ **Otros Componentes adicionales:**

-   **Executor:**  Encapsula la lógica de ejecución de las tareas.
-   **Trigger:**  Define cuándo se debe ejecutar una tarea.
-   **Sensor:**  Monitoriza un estado o condición antes de ejecutar una tarea.
-   **Variable:**  Almacena valores que se pueden usar en los flujos de trabajo.


## DAG y Task en Airflow:

**DAG (Directed Acyclic Graph / Grafo Acíclico Dirigido)** Es un conjunto de tareas que se ejecutan en un orden específico que pueden ejecutar en paralelo o en serie.

**Task:** es una unidad de trabajo individual dentro de un DAG.

* Ciclo de vida de un **Task**

* ![airflow_task_lifecycle](images/airflow_task_lifecycle_diagram.png)

## Operadores 

🔹 **BashOperator:**
Ejecuta un comando Bash en el worker, es el operador más utilizado para ejecutar tareas simples.

🔹 **PythonOperator:**
Permite ejecutar código Python más complejo.

🔹 **EmptyOperator (Anteriormente DummyOperator):**
No hace nada 😅... Se utiliza como marcador de posición (Generalmente iniciar y/o finalizar un Dag).

🔹 **HTTPOperator:**
Realiza una solicitud HTTP a una API o URL.

🔹 **EmailOperator:**
Envía un correo electrónico.

🔹 **S3Operator:**
Interactúa con el servicio de almacenamiento en la nube Amazon S3.

🔹 **BigQueryOperator:**
Interactúa con el servicio de análisis de datos Google BigQuery.

🔹 **SnowflakeOperator:**
Interactúa con el servicio de almacenamiento en la nube Snowflake.

🔹 **KubernetesPodOperator:**
Se utiliza para ejecutar tareas en un clúster de Kubernetes.

🔹 **DockerOperator:**
Ejecuta una imagen de Docker en el worker. Se utiliza para ejecutar tareas en un entorno aislado.

🔹 **TriggerDagRunOperator:**
Desencadena la ejecución de otro DAG. Se utiliza para crear flujos de trabajo dependientes.

🔹 **Otros operadores importantes:**

**FileSensor:**  Monitoriza un archivo para detectar cambios.

**TimeSensor:**  Espera a que se alcance una fecha o hora específica.

**BranchPythonOperator:**  Ejecuta una función de Python en el worker según una condición.

# Integracion de Airflow y Docker:

*  **Aislamiento:** Aislar las tareas de Airflow entre sí para evitar conflictos entre entornos.

*  **Portabilidad:** Mover fácilmente tus flujos de trabajo de Airflow entre diferentes entornos.

*  **Escalabilidad:** Escalar fácilmente tus flujos de trabajo de Airflow al añadir más contenedores.

*  **Complementación:** Airflow se encarga de la orquestación de tareas mientras que Docker se encarga del aislamiento y la ejecución de las tareas.


# 🚀 Despliegue de Airflow en Docker Compose 

Directorio por defecto de Airflow:  

       |───dags
       |   │
       |   |   dag_1.py
       |   |   dag_2.py
       |   |   ...
       |
       └───config
       |
       └───plugins
       |
       └───logs
           │
           └───dag_1
           |   │
           |   └───task_1
           |   |   │   datefile_1
           |   |   │   datefile_2
           |   |   │   ...
           |   |
           |   └───task_2
           |       │   datefile_1
           |       │   datefile_2
           |       │   ...
           |
           └───dag_2
               │   ...

[Documentacion Oficial](https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html) 

1️⃣ Para implementar Airflow en Docker, podemos usar como base el archivo [docker-compose.yaml.](https://airflow.apache.org/docs/apache-airflow/2.8.2/docker-compose.yaml)

``` shell
$ curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.8.2/docker-compose.yaml'
```

️🆙  ️ Iniciamos todos los componenetes de Airflow en contenedores 
``` shell
$ cd ../docker/airflow
$ docker compose --profile flower up
```

⤵️️ ️ Detenemos y eliminamos los servicio inicializado previamente 
``` shell
$ cd ../docker/airflow
$ docker compose --profile flower down
```

[Inicio](../README.md)
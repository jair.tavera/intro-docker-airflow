
# Introducción a Docker  
  
![Docker](images/docker.png)  
  
🔰 [Docker](https://www.docker.com/) es una plataforma de contenedorización que permite empaquetar, distribuir y ejecutar aplicaciones en entornos aislados llamados contenedores. Estos contenedores proporcionan una forma consistente de ejecutar aplicaciones en diferentes entornos, eliminando los problemas de dependencias y configuraciones.  
  
## Principales Características  
  

 1. **Contenedorización:**  
  
Docker utiliza contenedores para encapsular aplicaciones y sus dependencias, garantizando su ejecución de manera consistente en cualquier entorno.  
  
2. **Eficiencia de Recursos:**  
  
Los contenedores comparten el mismo kernel del sistema operativo anfitrión, lo que los hace más ligeros y eficientes en recursos en comparación con las máquinas virtuales.  
  
3. **Portabilidad:**  
  
Las imágenes de Docker son portátiles y pueden ejecutarse en cualquier sistema que tenga Docker instalado, independientemente de la infraestructura subyacente.  
  
4. **Escalabilidad:**  
  
Docker facilita la escalabilidad horizontal, permitiendo la ejecución de múltiples instancias de contenedores para gestionar cargas de trabajo.  
  
5. **Automatización:**  
  
Docker se integra con herramientas de automatización como Docker Compose y Kubernetes, simplificando la implementación y gestión de aplicaciones.  
  

## VM vs Docker

Docker permite empaquetar aplicaciones de forma más portable, ligera y escalable que las VM tradicionales.  

![VM vs Docker](images/vm_vs_docker.png)  

**Ventajas claves:**
  
- **Menor overhead y mayor densidad:** Los contenedores Docker utilizan el kernel del sistema operativo host y comparten los binarios y bibliotecas entre sí. Esto los hace mucho más ligeros que las VM completas, permitiendo ejecutar más contenedores por servidor.  
- **Arranque más rápido**: Al no tener que arrancar un SO completo, los contenedores Docker se inician muy rápidamente, en segundos en lugar de minutos para una VM. Esto permite escalar aplicaciones más rápido.  
- **Mejor portabilidad:** Las imágenes Docker se pueden ejecutar en cualquier lugar que tenga Docker instalado, sin importar el sistema operativo host. Las VM requieren más compatibilidad de SO.  
- **Gestión de versiones y rollback**: Las imágenes Docker se pueden versionar y rotular fácilmtiliza contenedores para encapsular aplicaciones y sus dependencias, garantizaente, permitiendo volver fácilmente a versiones anteriores. Esto facilita las actualizaciones y rollback.  
- **Microservicios nativos**: Docker facilita construir aplicaciones como conjuntos de microservicios independientes empaquetados en sus propios contenedores.  
- **Utilización de recursos**: Los contenedores comparten el kernel y se ejecutan como procesos aislados en espacios de usuario. Esto permite una mayor utilización de recursos.  

**Como funciona?**

![Como funciona](images/docker-explaint.png)

1.  El usuario ejecuta comandos docker a través del **CLI** (cliente).
2.  El **CLI** se comunica con **dockerd** a través de la **API** para ejecutar acciones.
3.  **dockerd** recibe las llamadas API, interpreta los comandos y gestiona los contenedores y otros objetos.
4.  **dockerd** envía la respuesta o estado al **CLI** a través de la **API**.
5.  El **CLI** muestra la salida al usuario.

De esta forma el usuario administra contenedores Docker a través del CLI, dockerd maneja toda la ejecución por detrás a través de la API.

### Imagenes y Contenedores  
  
![Imagenes y Contenedores](images/docker_build.png)  

Estructura de un archivo Dockerfile 

```dockerfile
# Establece la imagen base
FROM imagen_base:tag

# Mantenedor o información sobre el autor
LABEL maintainer="nombre_autor <correo_autor@example.com>"

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /ruta/del/directorio

# Copia archivos locales al sistema de archivos del contenedor
COPY <directorio_local> /<directorio_contenedor>

# Ejecuta comandos en el contenedor
RUN comando_1 \
    && comando_2 \
    && comando_3

# Expone puertos para ser utilizados fuera del contenedor
EXPOSE puerto_1 puerto_2

# Define variables de entorno
ENV NOMBRE_VARIABLE valor

# Argumentos: Similar a ENV, pero solo es tenido en cuenta en la construccion 
ARG variable_argumento

# Define un punto de entrada predeterminado para el contenedor
ENTRYPOINT ["comando_predeterminado"]

# Agrega metadatos al contenedor para indicar cómo debe ejecutarse
CMD ["argumento_1", "argumento_2"]

```

### Docker Compose  

Es una herramienta para definir y ejecutar aplicaciones Docker multi-contenedor, usando un 
archivo YAML para configurar los servicios/contenedores que conforman tu aplicación, sus relaciones y dependencias, los volúmenes de datos, redes, etc.

![Docker compose](images/docker_compose2.png)  

```yaml
services:

  db:
    build:
      context: .
      dockerfile: Dockerfile
    image: postgres_custom:13
    container_name: postgres_13
    restart: "no"
    volumes:
    - pgdata:/var/lib/postgresql/data

  adminer:
    container_name: adminer_4
    image: adminer
    restart: "no"
    ports:
      - 8880:8080

volumes:
  pgdata:

```



## Principales comandos.

### 1. docker

Para descargar una imagen de un registro/repositorio. Ejemplo Postgres 
```sh
$ docker pull postgres
```

Construye una imagen desde un Dockerfile
```sh
$ cd ../docker/postgres
$ docker build -t postgres_custom .
```

Ejecuta un contenedor desde una imagen `-p [PUERTO_HOST]:[PUERTO_CONTENEDOR]`
```sh
$ cd ../docker/postgres
$ docker run -p 5433:5432 postgres_custom
```
  
Lista los contenedores activos
```sh
$ docker ps
```

Lista las imágenes disponibles localmente 
```sh
$ docker images 
```
Elimina una imagen. Ejemplo:   
```sh
$ docker rmi postgres_custom 
```
Elimina un contenedor (Debe estar detenido). 
```sh
$ docker rm mi_contenedor 
```
Ejecuta un comando en un contenedor activo 
```sh
$ docker exec <mi_contenedor> ls 
```
Muestra informacion de un volumen especifico 
```sh
$ docker volume inspect <mi_volumen> 
```
Inicia sesión en un registro de imágenes Docker 
```sh
$ docker login 
```
Sube una imagen a un registro    
```sh
$ docker push <postgres_custom> 
```
Ver estadistica de los contenedores:

```sh
$ docker stats
```
```
CONTAINER ID   NAME              CPU %     MEM USAGE / LIMIT     MEM %     NET I/O      BLOCK I/O     PIDS
f64ec3bad5e7   vigorous_mayer    0.01%     31.35MiB / 15.42GiB   0.20%     866B / 0B    0B / 52.7MB   7
9dca6c0da83c   magical_bardeen   0.02%     31.43MiB / 15.42GiB   0.20%     1.3kB / 0B   0B / 53.2MB   7
```
  
### 2. docker compose

Descarga imágenes nuevas utilizadas en los servicios. 
```sh
$ docker-compose pull 
```
Crea e inicia los servicios definidos en el archivo **docker-compose.yml**.  
```sh
$ docker-compose up 
```
Detiene los servicios activos definidos en docker-compose.yml.  
```sh
$ docker-compose stop 
```
Inicia los servicios definidos en docker-compose.yml.  
```sh
$ docker-compose start 
```
Detiene y elimina los contenedores, redes e imágenes creadas por _docker-compose up_. 
```sh
$ docker-compose down 
```
Construye o vuelve a crear las imágenes de los servicios según el Dockerfile.  
```sh
$ docker-compose build 
```
Muestra los logs de los servicios.  
```sh
$ docker-compose logs 
```
Lista los contenedores activos de los servicios.  
```sh
$ docker-compose ps 
```
Ejecuta comandos en contenedores de servicio en ejecución.  
```sh
$ docker-compose exec 
```
Ejecuta un contenedor de servicio temporal para ejecutar un comando único.  
```sh
$ docker-compose run 
```

## Ventajas de Docker puede tener sobre Podman 

![Docker vs Podman](images/docker-podman.jpg)


- **Madurez y adopción** - Docker lleva más tiempo en el mercado y tiene mayor adopción y uso en producción. El ecosistema y soporte alrededor de Docker está más maduro.  
- **Facilidad de uso** - La arquitectura cliente-servidor de Docker facilita el uso y administración centralizada a través del daemon. Podman es más complejo al interactuar directamente con CLI.  
- **Ecosistema** - Existen más herramientas, integraciones, librerías y funcionalidades disponibles específicamente para Docker. Podman tiene menor ecosistema propio.  
- **Orquestación nativa** - Docker tiene buena integración nativa con Docker Compose y Docker Swarm para definición y orquestación de servicios. Podman depende más de Kubernetes.  
- **Networking** - El modelo de red de Docker facilita la conexión y descubrimiento entre contenedores de forma simple. Podman depende más de la red del host.  
- **Almacenamiento** - Docker soporta distintos storage drivers o capas de almacenamiento como overlay, aufs, btrfs, entre otros. Podman solo usa overlay.  
- **Interfaz** - La interfaz de línea de comandos (CLI) de Docker es intuitiva y permite administrar imágenes, contenedores y volúmenes. Podman CLI es menos amigable.  
- **Rendimiento** - En algunos escenarios, Docker puede tener mejor rendimiento que Podman en tiempos de construcción de imágenes y creación de contenedores.  
- **Integración con Plataformas en la Nube** Muchas plataformas en la nube, como AWS, Azure y Google Cloud, han integrado de manera nativa el soporte para Docker. Esto facilita la implementación y gestión de contenedores en estas plataformas.  

[Inicio](../README.md)  
CREATE SCHEMA IF NOT EXISTS usuarios;


CREATE TABLE usuarios
(
    id                 VARCHAR(10) NOT NULL PRIMARY KEY,
    nombre             VARCHAR(21) NOT NULL,
    direccion          VARCHAR(63) NOT NULL,
    telefono           VARCHAR(22) NOT NULL,
    correo_electronico VARCHAR(29) NOT NULL,
    fecha_nacimiento   DATE        NOT NULL
);

INSERT INTO usuarios(id, nombre, direccion, telefono, correo_electronico, fecha_nacimiento)
VALUES ('00000', 'Admin', '8397 Tamara Fields Apt. 190	Johnsonfort, AS 14773', '453-384-8524x74830',
        'admin@example.com', '1990-01-01');

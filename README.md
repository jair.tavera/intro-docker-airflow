
# Workshop de Introducción a Apache Airflow "Dockerizado"

En este taller, exploraremos dos herramientas poderosas que han transformado la manera en que desarrollamos, implementamos y gestionamos flujos de trabajo: Docker y Apache Airflow.

## Temas Centrales:

1. [Docker: Contenedorización Simplificada](doc/Docker.md)

2. [Apache Airflow: Orquestación y Automatización de Flujos de Trabajo](doc/Airflow.md)

3. **Demo**: Airflow y Docker en acción:

4. **Preguntas**